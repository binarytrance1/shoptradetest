// window.slate = window.slate || {};
// window.theme = window.theme || {};

// /*================ Slate ================*/
// // =require slate/a11y.js
// // =require slate/cart.js
// // =require slate/utils.js
// // =require slate/rte.js
// // =require slate/sections.js
// // =require slate/currency.js
// // =require slate/images.js
// // =require slate/variants.js

// /*================ Sections ================*/
// // =require sections/product.js

// /*================ Templates ================*/
// // =require templates/customers-addresses.js
// // =require templates/customers-login.js

// $(document).ready(function() {
//   var sections = new slate.Sections();
//   sections.register('product', theme.Product);

//   // Common a11y fixes
//   slate.a11y.pageLinkFocus($(window.location.hash));

//   $('.in-page-link').on('click', function(evt) {
//     slate.a11y.pageLinkFocus($(evt.currentTarget.hash));
//   });

//   // Target tables to make them scrollable
//   var tableSelectors = '.rte table';

//   slate.rte.wrapTable({
//     $tables: $(tableSelectors),
//     tableWrapperClass: 'rte__table-wrapper',
//   });

//   // Target iframes to make them responsive
//   var iframeSelectors =
//     '.rte iframe[src*="youtube.com/embed"],' +
//     '.rte iframe[src*="player.vimeo"]';

//   slate.rte.wrapIframe({
//     $iframes: $(iframeSelectors),
//     iframeWrapperClass: 'rte__video-wrapper'
//   });

//   // Apply a specific class to the html element for browser support of cookies.
//   if (slate.cart.cookiesEnabled()) {
//     document.documentElement.className = document.documentElement.className.replace('supports-no-cookies', 'supports-cookies');
//   }
// });


'use strict';

var navbar = document.getElementById("navbar");
var navbarToggle = document.getElementById("navbar-toggle");
var navPages = document.getElementById("nav-pages");
var body = document.getElementById("body");

// Toggle navbar and prevent body from scrolling when navbar is open
navbarToggle.addEventListener('click', function(){
  if (navbar.classList.contains('is-open')) {
    this.setAttribute('aria-expanded', 'false');
    navbar.classList.remove('is-open');
    body.classList.remove("no-scroll");

    this.classList.remove('is-open');

  } else {
    navbar.classList.add('is-open'); 
    this.setAttribute('aria-expanded', 'true');
    body.classList.add("no-scroll");

    this.classList.add('is-open');
  }
});


// Subtle nav animation on scroll
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
	if (document.body.scrollTop > 40 || document.documentElement.scrollTop > 40) {
		navbar.classList.add("is-scrolled");
	} else {
		navbar.classList.remove("is-scrolled");
	}
}
console.log("executed");

// Add active class to appropriate link acc to page url
var anchor = document.getElementsByClassName('nav-pages__link'),
    current = window.location;
    
for (var i = 0; i < anchor.length; i++) {
  if(anchor[i].href == current) {
      anchor[i].classList.add("active");
  }
}

